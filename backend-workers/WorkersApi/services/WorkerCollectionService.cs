﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkersApi.Models;
using WorkersApi.Settings;

namespace WorkersApi.services
{
    public class WorkerCollectionService : IWorkerCollectionService
    {
        private readonly IMongoCollection<Workers> _workers;

        public WorkerCollectionService(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _workers = database.GetCollection<Workers>(settings.WorkerCollectionName);
        }

        public async Task<bool> Create(Workers worker)
        {
            await _workers.InsertOneAsync(worker);
            return true;
        }


        public async Task<bool> Delete(Guid id)
        {
            var result = await _workers.DeleteOneAsync(worker => worker.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }


        public async Task<Workers> Get(Guid id)
        {
            return (await _workers.FindAsync(worker => worker.Id == id)).FirstOrDefault();
        }


        public async Task<List<Workers>> GetAll()
        {
            var result = await _workers.FindAsync(note => true);
            return result.ToList();

        }

        public async Task<bool> Update(Guid id, Workers worker)
        {
            worker.Id = id;
            var result = await _workers.ReplaceOneAsync(worker => worker.Id == id, worker);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _workers.InsertOneAsync(worker);
                return false;
            }

            return true;
        }
    }
}

