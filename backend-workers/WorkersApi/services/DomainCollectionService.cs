﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkersApi.Models;
using WorkersApi.Settings;

namespace WorkersApi.services
{
    public class DomainCollectionService : IDomainCollectionService
    {
        private readonly IMongoCollection<Domain> _domains;

        public DomainCollectionService(IMongoDBSettingsD settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _domains = database.GetCollection<Domain>(settings.DomainCollectionName);
        }

        public async Task<bool> Create(Domain domain)
        {
            await _domains.InsertOneAsync(domain);
            return true;
        }


        public async Task<bool> Delete(Guid id)
        {
            var result = await _domains.DeleteOneAsync(domain => domain.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }


        public async Task<Domain> Get(Guid id)
        {
            return (await _domains.FindAsync(domain => domain.Id == id)).FirstOrDefault();
        }


        public async Task<List<Domain>> GetAll()
        {
            var result = await _domains.FindAsync(domain => true);
            return result.ToList();

        }

        public async Task<bool> Update(Guid id, Domain domain)
        {
            domain.Id = id;
            var result = await _domains.ReplaceOneAsync(domain => domain.Id == id, domain);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _domains.InsertOneAsync(domain);
                return false;
            }

            return true;
        }
    }
}

