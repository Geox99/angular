﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkersApi.Models;

namespace WorkersApi.services
{
    public interface IDomainCollectionService : ICollectionService<Domain>
    {
    }
}
