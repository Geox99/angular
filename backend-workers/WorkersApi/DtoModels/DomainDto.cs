﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkersApi.DtoModels
{
    public class DomainDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
