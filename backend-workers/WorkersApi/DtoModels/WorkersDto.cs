﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkersApi.DtoModels
{
    public class WorkersDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DomainId { get; set; }
        public string ContactNumber { get; set; }
    }
}
