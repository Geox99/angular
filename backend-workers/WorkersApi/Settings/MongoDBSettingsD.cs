﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkersApi.Settings
{
    public class MongoDBSettingsD : IMongoDBSettingsD
    {
        public string DomainCollectionName { get ; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set ; }
    }
}
