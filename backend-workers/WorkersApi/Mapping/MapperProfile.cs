﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkersApi.DtoModels;
using WorkersApi.EntityModels;

namespace WorkersApi.Mapping
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Workers, WorkersDto>();
            CreateMap<WorkersDto, Workers>();
        }
  
    }
}
