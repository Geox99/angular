﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkersApi.Models;
using WorkersApi.services;

namespace WorkersApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WorkersController : Controller
    {
        IWorkerCollectionService _workerCollectionService;

        public WorkersController(IWorkerCollectionService workerCollectionService)
        {
            _workerCollectionService = workerCollectionService ?? throw new ArgumentNullException(nameof(workerCollectionService));

        }
        [HttpGet]
        public async Task<IActionResult> GetWorkers()
        {
            List<Workers> workers = await _workerCollectionService.GetAll();
            return Ok(workers);
        }
        [HttpPost]
        public async Task<IActionResult> CreateWorkers([FromBody] Workers worker)
        {
            if (worker == null)
            {
                return BadRequest("Worker cannot be null");
            }
            bool created = await _workerCollectionService.Create(worker);
            if (created)
            {
                return Ok(worker);
            }
            return NoContent();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByWorkerId(Guid id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var worker = await _workerCollectionService.Get(id);
            if (worker == null)
            {
                return NotFound();
            }
            return Ok(worker);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateWorker(Guid id, [FromBody] Workers worker)
        {
            if (worker == null)
            {
                return BadRequest("Worker cannot be null");
            }
            bool updated = await _workerCollectionService.Update(id, worker);

            if (updated)
            {
                return Ok(worker);
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWorker(Guid id)
        {
            var worker = await _workerCollectionService.Get(id);
            if (worker == null)
            {
                return NotFound();
            }
            await _workerCollectionService.Delete(worker.Id);

            return NoContent();
        }
    }
    }

