﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkersApi.Models;
using WorkersApi.services;

namespace WorkersApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DomainsController: Controller
    {
        IDomainCollectionService _domainCollectionService;

        public DomainsController(IDomainCollectionService domainCollectionService)
        {
            _domainCollectionService = domainCollectionService ?? throw new ArgumentNullException(nameof(domainCollectionService));

        }
        [HttpGet]
        public async Task<IActionResult> GeDomains()
        {
            List<Domain> domains = await _domainCollectionService.GetAll();
            return Ok(domains);
        }
        [HttpPost]
        public async Task<IActionResult> CreateDomains([FromBody] Domain domain)
        {
            if (domain == null)
            {
                return BadRequest("Domain cannot be null");
            }
            bool created = await _domainCollectionService.Create(domain);
            if (created)
            {
                return Ok(domain);
            }
            return NoContent();
        }
 
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDomain(Guid id, [FromBody] Domain domain)
        {
            if (domain == null)
            {
                return BadRequest("Domain cannot be null");
            }
            bool updated = await _domainCollectionService.Update(id, domain);

            if (updated)
            {
                return Ok(domain);
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDomain(Guid id)
        {
            var domain = await _domainCollectionService.Get(id);
            if (domain == null)
            {
                return NotFound();
            }
            await _domainCollectionService.Delete(domain.Id);

            return NoContent();
        }
    }
}

