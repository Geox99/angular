﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkersApi.EntityModels
{
    public class Domain
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
