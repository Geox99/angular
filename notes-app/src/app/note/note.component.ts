import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Note } from '../models/note';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit, OnChanges {
  @Input() selectedCategoryId: string;
  @Input() searchedTerm: string;

  
  notes: Note[];

  constructor(private noteService: NoteService,private router:Router) { }

  ngOnInit(): void {
    this.noteService.serviceCall();
    //this.notes=this.noteService.getNotes()
    this.noteService.getNotes().subscribe((result)=>{
      this.notes=result;
    })
  }
  ngOnChanges(): void {
    if (this.selectedCategoryId) {
     // this.notes = this.noteService.getFiltredNotes(this.selectedCategoryId);
     this.noteService.getFilteredNotes(this.selectedCategoryId).subscribe((result)=>{
       this.notes=result;
     })
    }
    //if(this.searchedTitle){
    //  this.notes=this.noteService.getSearchNotes(this.searchedTitle)
    //}
    if(this.searchedTerm) {
      this.noteService.getFilteredNotesSearch(this.searchedTerm).subscribe((result) => {this.notes = result});
      this.searchedTerm = "";
    }

  }
  editNote(id: string) {
    this.router.navigateByUrl('edit-note/' + id);

    if(this.selectedCategoryId == undefined)
      this.noteService.getNotes().subscribe((result) => this.notes = result);
    else
        this.noteService.getFilteredNotes(this.selectedCategoryId).subscribe((result) => this.notes = result);
  }
  deleteNote(id: string){
    this.noteService.deleteNote(id);
    this.noteService.getNotes().subscribe((result)=>{
      this.notes=result;
    })
  }


}
