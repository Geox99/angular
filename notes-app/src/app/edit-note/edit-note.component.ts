import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from '../models/category';
import { Note } from '../models/note';
import { FilterService } from '../services/filter.service';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.scss']
})
export class EditNoteComponent implements OnInit {

  @Input() noteId: string;

  title: string;
  description: string;
  categories: Category[];
  idCategoryNote: string;
  id: string;

  note: Note;

  constructor(private filterService: FilterService, private noteService: NoteService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.categories = this.filterService.getFilters();
    this.id = this.route.snapshot.paramMap.get('id');
    this.title = this.note.title;
    this.description = this.note.description;
    this.idCategoryNote = this.note.categoryId;
  }
  edit() {
  
    this.noteService.editNote(this.id, this.title, this.description, this.idCategoryNote);
    this.router.navigateByUrl('');
  }
  
}
