import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'add'
})
export class AddPipe implements PipeTransform {

  transform(value: number, addvalue?: number): number {
    if(isNaN(addvalue))
    {
        return value;
    }
    else{
      return value+addvalue;
    }
  }
}
