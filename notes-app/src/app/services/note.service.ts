import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Note } from '../models/note';
import { map } from "rxjs/operators";


@Injectable()
export class NoteService {

  readonly baseUrl = "https://localhost:4200";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };



  constructor(private httpClient: HttpClient) { }
  serviceCall() {
    console.log("Note service was called");
  }

  getNotes(): Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl + '/notes', this.httpOptions)
  }
  //addNotes(note:Note){
  //this.notes.push(note)
  // }
  getFilteredNotes(categoryId: string): Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl + '/notes', this.httpOptions)
      .pipe(
        map((notes) => notes.filter((note) => note.categoryId === categoryId)))
    // return this.notes.filter(note=>note.categoryId===categoryId)
  }
  // getSearchNotes(title: string){
  //return this.notes.filter(note=>note.title===title)
  // }
  getFilteredNotesSearch(searchTerm: string): Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl+'/notes', this.httpOptions).pipe(map((notes) => notes.filter((note) => note.title.toLowerCase().includes(searchTerm.toLowerCase()) || note.description.toLowerCase().includes(searchTerm.toLowerCase()))));
  }

  getNoteById(id: string): Observable<Note> {
    return this.httpClient.get<Note>(this.baseUrl+'/notes/'+id, this.httpOptions);
  }

  addNote(noteTitle: string, noteDescription: string, noteCategoryId: string) {
    let note = {
      description: noteDescription,
      title: noteTitle,
      categoryId: noteCategoryId
    }
    return this.httpClient.post(this.baseUrl + "/note", note, this.httpOptions).subscribe();
  }

  deleteNote(id: string) {
    return this.httpClient.delete(this.baseUrl + "/note/" + id).subscribe();
  }
  editNote(noteId, noteTitle, noteDescription, noteStatus) {
    let note = {
      id: noteId,
      title: noteTitle,
      description: noteDescription,
      categoryId: noteStatus
      };
      return this.httpClient.put(this.baseUrl+"/notes/"+noteId, note, this.httpOptions).subscribe();
  }
}
