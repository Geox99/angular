﻿using NotesApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesApi.Services
{
    public class OwnerCollectionService : IOwnerCollectionService
    {
        private List<Owner> _owners = new List<Owner>
        {
            new Owner { Id=Guid.NewGuid(), Name="First Owner"},
            new Owner {Id=Guid.NewGuid(), Name="Second Owner"},
            new Owner {Id=Guid.NewGuid(), Name="Third Owner"}
        };
        public bool Create(Owner model)
        {
            _owners.Add(model);
            bool isAdded = _owners.Contains(model);
            return isAdded;
        }

        public bool Delete(Guid id)
        {
            Owner ownerToDelete = _owners.FirstOrDefault(owner => owner.Id == id);
            if (ownerToDelete == null)
            {
                return false;
            }
            bool isRemoved = _owners.Remove(ownerToDelete);
            return isRemoved;
        }

        public Owner Get(Guid id)
        {
            return _owners.FirstOrDefault(owner => owner.Id == id);
        }

        public List<Owner> GetAll()
        {
           return _owners;
        }

        public bool Update(Guid id, Owner model)
        {
            int index = _owners.FindIndex(owner => owner.Id == id);
            model.Id = _owners[index].Id;
            _owners[index] = model;
            bool isUpdated = _owners.Contains(model);
            return isUpdated;
        }
    }
}
