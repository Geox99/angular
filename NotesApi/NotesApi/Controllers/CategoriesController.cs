﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoriesController: ControllerBase
    {
        List<Category> categories = new List<Category>()
        {
            new Category{ Id="1", Name="To Do"},
            new Category{ Id="2", Name="Done"},
            new Category{ Id="3", Name="Doing"}
        };

        public CategoriesController()
        {

        }

        /// <summary>
        ///  Returns a list of categories.
        /// </summary>
        /// <response code="400">Bad request</response>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult Get()
        {
            return Ok(categories);
        }

        /// <summary>
        ///     Returns a category with a specific id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult GetWithParams(string id)
        {
            Category category = categories.FirstOrDefault(e => e.Id == id);
            if(category==null)
            {
                return NotFound();
            }
            return Ok(category);
        }

        /// <summary>
        ///  Creates a list of categories
        /// </summary>
        /// <response code="400">Bad request</response>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Post()
        {
            return Ok(this.categories);
        }
        /// <summary>
        ///  Deletes a category with a specific id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var itemToRemove = this.categories.FirstOrDefault(r => r.Id == id);
            if(itemToRemove==null)
            {
                return NotFound();
            }
            else
            {
                this.categories.Remove(itemToRemove);
            }
            return Ok(this.categories);
        }
    }
}
