﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NotesApi.Models;
using NotesApi.Services;

namespace NotesApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OwnerController : ControllerBase
    {
        IOwnerCollectionService _ownerCollectionService;

        public OwnerController(IOwnerCollectionService ownerCollectionService)
        {
            _ownerCollectionService = ownerCollectionService ?? throw new ArgumentNullException(nameof(ownerCollectionService));
        }

        [HttpGet]
        public IActionResult GetOwners()
        {
            List<Owner> owners = _ownerCollectionService.GetAll();
            return Ok(owners);
        }

        [HttpPost]

        public IActionResult CreateOwners([FromBody] Owner owner)
        {
            if (owner == null)
            {
                return BadRequest("Owner cannot be null");
            }
            
            bool created = _ownerCollectionService.Create(owner);
            if (created)
            {
                return Ok(owner);
            }

            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult UpdateNote(Guid id, [FromBody] Owner owner)
        {
            if (owner == null)
            {
                return BadRequest("Note cannot be null");
            }
            bool updated = _ownerCollectionService.Update(id, owner);

            if (updated)
            {
                return Ok(owner);
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteNote(Guid id)
        {
            var owner = _ownerCollectionService.Get(id);
            if (owner == null)
            {
                return NotFound();
            }
            _ownerCollectionService.Delete(owner.Id);

            return NoContent();
        }
    }
}

