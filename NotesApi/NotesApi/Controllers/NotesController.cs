﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using NotesApi.Models;
using NotesApi.Services;

namespace NotesApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotesController : ControllerBase
    {
        INoteCollectionService _noteCollectionService;

        public NotesController(INoteCollectionService noteCollectionService) 
        {
            _noteCollectionService = noteCollectionService ?? throw new ArgumentNullException(nameof(noteCollectionService));

        }

        [HttpGet]
        public IActionResult GetNotes()
        {
            List<Notes> notes = _noteCollectionService.GetAll();
            return Ok(notes);

        }
        [HttpPost]
        public IActionResult CreateNotes([FromBody] Notes note)
        {
            if (note == null)
            {
                return BadRequest("Note cannot be null");
            }
            bool created=_noteCollectionService.Create(note);
            if(created)
            {
                return Ok(note);
            }

            return NoContent();
        }
        [HttpGet("{id}")]
        public IActionResult GetByNoteId(Guid id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var note = _noteCollectionService.Get(id);
            if (note == null)
            {
                return NotFound();
            }
            return Ok(note);
        }
        [HttpPut("{id}")]
        public IActionResult UpdateNote(Guid id, [FromBody] Notes note)
        {
            if (note == null)
            {
                return BadRequest("Note cannot be null");
            }
            bool updated = _noteCollectionService.Update(id, note);

            if (updated)
            {
                return Ok(note);
            }
            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteNote(Guid id)
        {
            var note = _noteCollectionService.Get(id);
            if(note==null)
            {
                return NotFound();
            }
            _noteCollectionService.Delete(note.Id);

            return NoContent();
        }
        [HttpGet("Owner/{id}")]
        public IActionResult GetByOwnerId(Guid id)
        {
            List<Notes> note = _noteCollectionService.GetNotesByOwnerId(id);
            return Ok(note);
        }

        //[HttpGet("OwnerId/{id}")]
        //public IActionResult GetByOwnerId(Guid id)
        //{
        //    List<Notes> note = _notes.FindAll(note => note.OwnerId == id);
        //    if (note == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(note);
        //}

        //[HttpGet("NoteId/{id}")]
        //public IActionResult GetByNoteId(Guid id)
        //{
        //    Notes note = _notes.Find(note => note.Id == id);
        //    if (note == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(note);

        //}

        //[HttpGet("/id", Name = "GetNote")]
        //public ActionResult<Notes> Get(Guid id)
        //{
        //    Notes note = _notes.Find(note => note.Id == id);
        //    if (note == null)
        //    {
        //        return NotFound();
        //    }
        //    return note;
        //}

        //[HttpPost]
        //public IActionResult CreateNotes([FromBody] Notes note)
        //{
        //    if (note == null)
        //    {
        //        return BadRequest("Note cannot be null");
        //    }
        //    _notes.Add(note);

        //    return Ok();
        //}

        //[HttpPost]
        //public ActionResult<Notes> Create(Notes note)
        //{
        //    if (note == null)
        //    {
        //        return BadRequest("Note cannot be null");
        //    }
        //    _notes.Add(note);

        //    return CreatedAtRoute("GetNote", new { id = note.Id }, note);
        //}

        //[HttpPut("{id}")]
        //public IActionResult UpdateNote(Guid id, [FromBody] Notes note)
        //{
        //    if (note == null)
        //    {
        //        return BadRequest("Note cannot be null");
        //    }
        //    int index = _notes.FindIndex(note => note.Id == id);
        //    note.Id = _notes[index].Id;
        //    _notes[index] = note;

        //    return Ok(_notes[index]);
        //}

        //[HttpPut("{ownerId}")]
        //public IActionResult UpdateNoteByOwner(Guid id,Guid ownerId,[FromBody] Notes note)
        //{
        //    int index;
        //    if(note ==null)
        //    {
        //        return BadRequest("Note cannot be null");
        //    }
        //    if(note == _notes.FirstOrDefault(note => note.Id == id && note.OwnerId == ownerId))
        //    {
        //        index = _notes.FindIndex(note => note.Id == id);
        //        note.Id = _notes[index].Id;
        //        note.OwnerId = _notes[index].OwnerId;
        //        _notes[index] = note;
        //        return Ok(_notes[index]);
        //    }

        //    return NotFound();

        //}

        //[HttpDelete("{id}/OwnerId")]
        //public IActionResult DeleteNoteByOwnerId(Guid id, Guid ownerId)
        //{
        //    Notes note = _notes.FirstOrDefault(note => note.Id == id && note.OwnerId == ownerId);
        //    if (note == null)
        //    {
        //        return BadRequest("Note cannot be null");
        //    }
        //    _notes.Remove(note);
        //    return NoContent();
        //}
        //[HttpDelete("{id}/OwnerId")]
        //public IActionResult DeleteAllByOwnerId(Guid ownerId)
        //{
        //    _notes.RemoveAll(note => note.OwnerId == ownerId);
        //    return NoContent();
        //}
        //[HttpDelete("{id}")]
        //public IActionResult DeleteNote(Guid id)
        //{
        //    var index = _notes.FindIndex(note => note.Id == id);
        //    if (index == -1)
        //    {
        //        return NotFound();
        //    }
        //    _notes.RemoveAt(index);

        //    return NoContent();
        //}

        //[HttpPatch("{id}/title")]
        //public IActionResult UpdateTitleNote(Guid id,[FromBody] string title)
        //{
        //    if(string.IsNullOrEmpty(title))
        //    {
        //        return BadRequest("The string cannot be null");
        //    }
        //    var index = _notes.FindIndex(note => note.Id == id);
        //    if(index==-1)
        //    {
        //        return NotFound();
        //    }
        //    _notes[index].Title = title;
        //    return Ok(_notes[index]);
        //}

        //[HttpDelete("{id}")]

        //public IActionResult DeleteNote(Guid id)
        //{
        //    Notes noteToDelete = _notes.FirstOrDefault(note => note.Id == id);
        //    if(noteToDelete==null)
        //    {
        //        return NotFound();
        //    }
        //    bool isRemoved = _notes.Remove(noteToDelete);
        //    if(isRemoved)
        //    {
        //        return Ok();
        //    }
        //    return NotFound();
        //}






    }
}
