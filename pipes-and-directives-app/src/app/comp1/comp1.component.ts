import { Component, OnInit } from '@angular/core';
import { Cities } from '../models/cities';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.scss']
})
export class Comp1Component implements OnInit {

  cities: Cities[]=[
    {id:'1',
    name:'Galati'},
    {
      id:'2',
      name:'Braila'
    },
    {
      id:'3',
      name:'Brasov'
    },
    {
      id:'4',
      name:'Ploiesti'
    },
  ]

 
  constructor() { }

  ngOnInit(): void {
  }

}
