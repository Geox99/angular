import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Cities } from './models/cities';
import { DatePipe } from '@angular/common';
import { Comp1Component } from './comp1/comp1.component';
import { DecreasePipe } from './decrease.pipe';
import { ChangeColorDirective } from './change-color.directive';

@NgModule({
  declarations: [
    AppComponent,
    Comp1Component,
    DecreasePipe,
    ChangeColorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {

 

 }
