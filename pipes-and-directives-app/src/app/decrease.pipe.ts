import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decrease'
})
export class DecreasePipe implements PipeTransform {

  transform(value: number, decreasevalue?:number): number {
    if(isNaN(decreasevalue))
    {
        return value;
    }
    else{
      return value-decreasevalue;
    }
  }
}
