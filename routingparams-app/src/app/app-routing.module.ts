import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoteDetailsComponent } from './note-details/note-details.component';
import { NoteComponent } from './note/note.component';

const routes: Routes = [
  {path: "",component:NoteComponent,pathMatch: 'full'},
  {path: "notedetails",component:NoteDetailsComponent},
  {path: "**",redirectTo:''}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
