import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Worker } from '../models/worker';
import { WorkerService } from '../services/worker.service';

@Component({
  selector: 'app-worker',
  templateUrl: './worker.component.html',
  styleUrls: ['./worker.component.scss']
})
export class WorkerComponent implements OnInit {
  @Input() selectedDomainId: string;
  @Input() searchedTerm: string;

  workers: Worker[];
  constructor(private workerService: WorkerService,private router:Router) { }

  ngOnInit(): void {
    this.workerService.serviceCall();
    //this.notes=this.noteService.getNotes()
    this.getWorkers();
    this.searchedTerm="";
  }
  ngOnChanges(): void {
    if (this.selectedDomainId) {
     // this.notes = this.noteService.getFiltredNotes(this.selectedCategoryId);
     this.workerService.getFilteredWorkers(this.selectedDomainId).subscribe((result)=>{
       this.workers=result;
     })
    }
    //if(this.searchedTitle){
    //  this.notes=this.noteService.getSearchNotes(this.searchedTitle)
    //}
    if(this.searchedTerm) {
      this.workerService.getFilteredWorkersSearch(this.searchedTerm).subscribe((result) => {this.workers = result});
      console.log("Searched term: " + this.searchedTerm);
      //this.searchedTerm = "";
    }

  }
  editWorker(id: string) {
    this.router.navigateByUrl('edit-worker/' + id);

    if(this.selectedDomainId == undefined)
      this.workerService.getWorkers().subscribe((result) => this.workers = result);
    else
        this.workerService.getFilteredWorkers(this.selectedDomainId).subscribe((result) => this.workers = result);
  }
  deleteWorker(id: string){
    this.workerService.deleteWorker(id).subscribe(()=>this.getWorkers());
    
  }

  getWorkers()
  {
    this.workerService.getWorkers().subscribe((result)=>{
      this.workers=result;
    })
  }

}
