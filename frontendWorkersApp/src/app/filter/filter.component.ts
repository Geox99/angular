import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Domain } from '../models/domain';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  domains:Domain[]
  @Output() emitSelectedFilter = new EventEmitter<string>();
  constructor(private filterService:FilterService ) { }

  ngOnInit(): void {
   this.getFilters()
  }
  getFilters() {
    this.filterService.getFilters().subscribe((result)=>{
      this.domains=result;
    })
  }
  selectFilter(domainId: string) {
    this.emitSelectedFilter.emit(domainId);
  }

}
