import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Domain } from '../models/domain';
import { Worker } from '../models/worker';
import { FilterService } from '../services/filter.service';
import { WorkerService } from '../services/worker.service';

@Component({
  selector: 'app-edit-worker',
  templateUrl: './edit-worker.component.html',
  styleUrls: ['./edit-worker.component.scss']
})
export class EditWorkerComponent implements OnInit {

  @Input() workerId: string;

  name: string;
  description: string;
  domains: Domain[];
  idDomainWorker: string;
  id: string;
  contactNumber: String;
  
  worker: Worker;

  constructor(private filterService: FilterService, private workerService: WorkerService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.id = this.route.snapshot.paramMap.get('id');
    this.name = this.worker.name;
    this.description = this.worker.description;
    this.idDomainWorker = this.worker.domainId;
    this.contactNumber=this.worker.contactNumber;
    this.getFilters();
  }
  getFilters() {
    this.filterService.getFilters().subscribe((result)=>{
      this.domains=result;
    })
  }
  edit() {
  
    this.workerService.editWorker(this.id, this.name, this.description, this.idDomainWorker,this.contactNumber).subscribe(()=> {this.router.navigateByUrl('')});
   
  }
}
