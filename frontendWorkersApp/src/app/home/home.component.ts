import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  searchedTerm: string;
  domainId: string; 

  constructor() { }

  ngOnInit(): void {
  }

  receiveDomain(domId: string) {
    this.domainId = domId;
  }
  receiveSearchedTerm(searchTerm: string) {
    this.searchedTerm = searchTerm;
  }

}
