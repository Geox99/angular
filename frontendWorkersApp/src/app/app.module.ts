import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WorkerComponent } from './worker/worker.component';
import { AddWorkerComponent } from './add-worker/add-worker.component';
import { EditWorkerComponent } from './edit-worker/edit-worker.component';
import { FilterComponent } from './filter/filter.component';
import { ToolsComponent } from './tools/tools.component';
import { WorkerService } from './services/worker.service';
import {MatInputModule} from '@angular/material/input';
import { MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule } from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from "@angular/material/card";
import { HomeComponent } from './home/home.component';
import {MatSelectModule} from '@angular/material/select';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FilterService } from './services/filter.service';



@NgModule({
  declarations: [
    AppComponent,
    WorkerComponent,
    AddWorkerComponent,
    EditWorkerComponent,
    FilterComponent,
    HomeComponent,
    SearchBarComponent,
    ToolsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    MatCardModule,
    MatSelectModule,
    CommonModule,
    HttpClientModule
  ],
  providers: [ WorkerService,FilterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
