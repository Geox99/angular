import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Worker } from '../models/worker';
import { map } from "rxjs/operators";

@Injectable()
export class WorkerService {
  readonly baseUrl = "http://localhost:5000";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private httpClient: HttpClient) { }
  serviceCall() {
    console.log("Worker service was called");
  }

  getWorkers(): Observable<Worker[]> {
    return this.httpClient.get<Worker[]>(this.baseUrl + '/Workers', this.httpOptions)
  }
  //addNotes(note:Note){
  //this.notes.push(note)
  // }
  getFilteredWorkers(domainId: string): Observable<Worker[]> {
    return this.httpClient.get<Worker[]>(this.baseUrl + '/Workers', this.httpOptions)
      .pipe(
        map((workers) => workers.filter((worker) => worker.domainId === domainId)))
    // return this.notes.filter(note=>note.categoryId===categoryId)
  }
  // getSearchNotes(title: string){
  //return this.notes.filter(note=>note.title===title)
  // }
  getFilteredWorkersSearch(searchTerm: string): Observable<Worker[]> {
    return this.httpClient.get<Worker[]>(this.baseUrl+'/Workers', this.httpOptions).pipe(map((workers) => workers.filter((worker) => worker.name.toLowerCase().includes(searchTerm.toLowerCase()) || worker.description.toLowerCase().includes(searchTerm.toLowerCase()))));
  }


  addWorker(workerName: string, workerDescription: string, workerDomainId: string,contactNumber:string): Observable<Worker> {
    let worker = {
      description: workerDescription,
      name: workerName,
      workerDomainId: workerDomainId,
      contactNumber: contactNumber
    }
    return this.httpClient.post<Worker>(this.baseUrl + "/Workers", worker, this.httpOptions);
  }

  deleteWorker(id: string) {
    return this.httpClient.delete(this.baseUrl + "/Workers/" + id,this.httpOptions);
  }
  editWorker(workerId, workerName, workerDescription, workerStatus,contactNumber) : Observable<Worker> {
    let worker = {
      id: workerId,
      name: workerName,
      description: workerDescription,
      domainId: workerStatus,
      contactNumber: contactNumber
      };
      return this.httpClient.put<Worker>(this.baseUrl+"/Workers/"+workerId, worker, this.httpOptions);
  }
}
