import { Injectable } from '@angular/core';
import { Domain } from '../models/domain';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class FilterService {

  readonly baseUrl = "http://localhost:5000";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };
  constructor(private httpClient: HttpClient) { console.log("Worker service was called"); }
  getFilters(): Observable<Domain[]> {
    return this.httpClient.get<Domain[]>(this.baseUrl + '/Domains', this.httpOptions)
  }
}
