import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Domain } from '../models/domain';
import { FilterService } from '../services/filter.service';
import { WorkerService } from '../services/worker.service';

@Component({
  selector: 'app-add-worker',
  templateUrl: './add-worker.component.html',
  styleUrls: ['./add-worker.component.scss']
})
export class AddWorkerComponent implements OnInit {
  name: string;
  description: string;
  idDomainWorker: string;
  contactNumber: string;

  domains: Domain[];
  constructor(private router: Router, private filterService: FilterService, private workerService: WorkerService ){}

  ngOnInit(): void {
    this.getFilters();
  }
  getFilters() {
    this.filterService.getFilters().subscribe((result)=>{
      this.domains=result;
    })
  }
  add() {
    this.workerService.addWorker(this.name,this.description,this.idDomainWorker,this.contactNumber)
    .subscribe(()=>{this.router.navigateByUrl('')});
  }
}
