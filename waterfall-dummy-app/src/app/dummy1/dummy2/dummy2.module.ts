import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Comp2Component } from './comp2/comp2.component';
import { Dummy3Module } from './dummy3/dummy3.module';
import { Comp4Component } from './dummy3/dummy4/comp4/comp4.component';
import { Comp3Component } from './dummy3/comp3/comp3.component';



@NgModule({
  declarations: [
    Comp2Component,
  ],
  imports: [
    CommonModule,
    Dummy3Module
  ],
  exports: [Comp2Component,Comp4Component,Comp3Component]
})
export class Dummy2Module { }
