import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Comp3Component } from './comp3/comp3.component';
import { Comp4Component } from './dummy4/comp4/comp4.component';
import { Dummy4Module } from './dummy4/dummy4.module';



@NgModule({
  declarations: [
    Comp3Component
  ],
  imports: [
    CommonModule,
    Dummy4Module
  ],
  exports: [Comp3Component,Comp4Component]
})
export class Dummy3Module { }
