import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Comp4Component } from './comp4/comp4.component';



@NgModule({
  declarations: [
    Comp4Component
  ],
  imports: [
    CommonModule
  ],
  exports: [Comp4Component]
})
export class Dummy4Module { }
