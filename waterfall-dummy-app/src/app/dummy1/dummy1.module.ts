import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Comp1Component } from './comp1/comp1.component';
import { Comp4Component } from './dummy2/dummy3/dummy4/comp4/comp4.component';
import { Dummy2Module } from './dummy2/dummy2.module';



@NgModule({
  declarations: [
    Comp1Component
  ],
  imports: [
    CommonModule,
    Dummy2Module
  ],
  exports: [Comp1Component,Comp4Component]
})
export class Dummy1Module { }
